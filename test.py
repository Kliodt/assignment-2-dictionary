import subprocess

tests = [ #  input    expected_output     expected_errors
    ["first", "first - first word explanation", ""],
    ["not", "", "Error: Word not found"],
    ["", "", "Error: Given string too long or empty"],
    ["x"*255, "", "Error: Word not found"],
    ["x"*256, "", "Error: Given string too long or empty"]
]


#proc = subprocess.call("./program <tmp2 2>>tmp3")

def check(test):
    inp = test[0]
    outp = test[1]
    err = test[2]
    result = subprocess.run("./program", capture_output=True, input=inp.encode())
    return result.stderr.decode().replace("\n", "") == err and result.stdout.decode().replace("\n", "") == outp


err_count = 0
for test in tests:
    if check(test):
        print(".", end="")
    else:
        err_count += 1
        print("F", end="")
if err_count:
    print("\n" + str(err_count) + " errors")
else:
    print("\nOK")
