ASM=nasm
ASMFLAGS=-felf64
LD=ld

.PHONY: clean
clean: 
	rm *.o program

.PHONY: test
test:
	python3 test.py

main.o: main.asm dict.inc lib.inc words.inc
lib.o: lib.asm
dict.o: dict.asm lib.inc colon.inc

%.o: %.asm
	$(ASM) -g $< $(ASMFLAGS) -o $@

program: main.o lib.o dict.o
	$(LD) -o $@ $^