%include "dict.inc"
%include "lib.inc"
%include "words.inc"

%define BUFFER_LENGTH 256

global _start

section .rodata
DASH: db " - ", 0
LENGTH_ERROR: db "Error: Given string too long or empty", 10, 0
DICTIONARY_ERROR: db "Error: Word not found", 10, 0

section .text

_start:
    sub rsp, BUFFER_LENGTH
    mov rdi, rsp
    mov rsi, BUFFER_LENGTH

    call read_word
    test rax, rax
    jz .too_long_string

    mov rdi, rax
    mov rsi, NEXT
    call find_word
    test rax, rax
    jz .word_not_found

    mov rdi, rax                ;напечатать слово
    push rdi
    call print_string           ;возвращает количество напечатанных символов
    pop rdi
    add rax, rdi
    inc rax

    push rax                    ;напечатать "-"
    mov rdi, DASH
    call print_string

    pop rdi                     ;напечатать определение
    call print_string
    call print_newline
    jmp .end

.too_long_string:
    mov rdi, LENGTH_ERROR
    call print_error
    jmp .end

.word_not_found:
    mov rdi, DICTIONARY_ERROR
    call print_error

.end:
    mov rdi, 0
    call exit