%include "lib.inc"

global find_word

section .text

; Принимает указатель на 0-терминированную строку и указатель на начало словаря
; Возвращает адрес начала вхождения в словарь или 0, если вхождений нет
find_word:
    push r12
    push r13
    
    mov r12, rdi        ;указатель на строку для поиска
    mov r13, rsi        ;указатель на текущий элемент словаря (указатель на указатель следующего элемента)
.loop:
    test r13, r13
    jz .not_found

    mov rdi, r12
    
    mov rsi, r13        ;r13+8 - указатель на ключ
    add rsi, 8
    call string_equals
    test rax, rax

    jnz .found  

    mov r13, [r13]
    
    jmp .loop

.not_found:
    xor rax, rax
    pop r13
    pop r12
    ret

.found:
    mov rax, r13
    add rax, 8
    pop r13
    pop r12
    ret
