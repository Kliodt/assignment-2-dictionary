%define NEXT 0

; Принимает ключ и имя метки
%macro colon 2
    %%CURRENT:
    dq NEXT                     ;8 байт - ссылка на след. элемент
    %define NEXT %%CURRENT
    db %1, 0                       ;key (0-terminated string)  
    %2:                         
%endmacro